import cv2 as cv
import numpy as np


def findBestPath(image, start, destiny):
    # Output image in RGB so the path can be drawn in red.
    out = np.repeat(image[..., None], 3, axis = 2)

    # Set current location to start pixel.
    current = start
    cy, cx = current

    while current != destiny:
        # Paint the current pixel red to form the path.
        out[cy, cx] = [0, 0, 255]

        # Check each neighbor's distance from destiny.
        neighbors = np.array([(cy-1, cx), (cy-1, cx-1),
                              (cy+1, cx), (cy+1, cx-1),
                              (cy, cx-1), (cy-1, cx+1),
                              (cy, cx+1), (cy+1, cx+1)])
        
        dists = np.linalg.norm(neighbors - destiny, axis = 1)

        # Indices of distances sorted.
        distsInds = np.argsort(dists)

        # Pick the three neighbors nearest to destiny.
        possible_choices = np.array([neighbors[distsInds[0]],
                                     neighbors[distsInds[1]],
                                     neighbors[distsInds[2]]])

        # These lines were necessary to convert possible choices into an array o tuples.
        possible_choices = np.array([tuple(i) for i in possible_choices], dtype='int,int')
        possible_choices = possible_choices.astype(tuple)
        
        # Take from possible choices the co-ordinate of the
        # neighbor with least bright level on the image.
        best_ind = np.argsort([image.item(possible_choices[0]),
                               image.item(possible_choices[1]),
                               image.item(possible_choices[2])])[0]

        best_choice = possible_choices[best_ind]

        # Move to best choice.
        current = tuple(best_choice)
        cy, cx = current

    return out


start = (260, 415)
destiny = (815, 1000)

# Loading mars image.
MRGB = cv.imread('images/Mars.bmp', -1)

# Turning into grayscale.
MGray = np.mean(MRGB, axis = 2).astype(np.uint8)

# Equalizing image.
MHeq = cv.equalizeHist(MGray)

MPath = findBestPath(MHeq, start, destiny)

cv.namedWindow('path', cv.WINDOW_NORMAL)
cv.imshow('path', MPath)

cv.waitKey(0)