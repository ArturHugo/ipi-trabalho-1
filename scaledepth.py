import cv2 as cv
import numpy as np


def isGrayScale(image):
    if len(image.shape) == 2:
        return True
    else:
        return False

def interpolate(image, scale, x, y):
    h = image.shape[0]
    w = image.shape[1]

    if scale > 1:
        x1 = int(np.floor(x/scale))
        y1 = int(np.floor(y/scale))

        if x1 < w/2:
            x2 = x1 + 1
        else:
            x2 = x1 - 1
        
        if y1 < h/2:
            y2 = y1 + 1
        else:
            y2 = y1 - 1

        fq = [[image.item(y1, x1), image.item(y1, x2)], [image.item(y2, x1), image.item(y2, x2)]]
        x1 = int(np.ceil(x1*scale))
        x2 = int(np.ceil(x2*scale))
        y1 = int(np.ceil(y1*scale))
        y2 = int(np.ceil(y2*scale))
        delta_x = [x2-x, x-x1]
        delta_y = np.transpose([y2-y, y-y1])
        const = 1/((x2-x1)*(y2-y1))
        temp = const * np.matmul(delta_x, fq)
        return np.matmul(temp, delta_y)

    else:
        offset = int(np.floor(1/(2*scale*scale)))
        x0 = int(np.floor(x/scale))
        y0 = int(np.floor(y/scale))

        if x0 - offset < 0:
            dif = offset - x0
            x1 = 0
            x2 = x0 + offset + dif
        elif x0 + offset >= w:
            dif = x0 + offset - w + 1
            x2 = w - 1
            x1 = x0 - offset - dif
        else:
            x1 = x0 - offset
            x2 = x0 + offset

        if y0 - offset < 0:
            dif = offset - y0
            y1 = 0
            y2 = y0 + offset + dif
        elif y0 + offset >= h:
            dif = y0 + offset - h + 1
            y2 = h - 1
            y1 = y0 - offset - dif
        else:
            y1 = y0 - offset
            y2 = y0 + offset

        return np.mean(image[y1:y2, x1:x2])


def im_chscaledepth(image, depth, scale):
    # rf and cf correspond to the final dimensions.
    rows = image.shape[0]
    cols = image.shape[1]
    rf   = int(np.ceil(rows*scale))
    cf   = int(np.ceil(cols*scale))
    image = image.astype(int)

    # Change scale aplying interpolation algorithm.
    if scale != 1:
        if isGrayScale(image):
            new_image = np.zeros((rf, cf))
            for y in range(0, rf):
                    for x in range(0, cf):
                        new_image[y, x] = interpolate(image, scale, x, y)
        else:
            new_image = np.zeros((rf, cf, image.shape[2]))
            for channel in range (0, image.shape[2]):
                for y in range(0, rf):
                    for x in range(0, cf):
                        new_image[y, x, channel] = interpolate(image[:,:,channel], scale, x, y)

        # Cap values out of range.
        new_image = np.where(new_image > 255, 255, new_image)
        new_image = np.where(new_image < 0, 0, new_image)

        # Make a low-pass filtering to correct some noise after upscaling.
        if scale > 1:
            size = int(np.round(2*scale))
            kernel = np.ones((size,size),np.float32)/(size*size)
            new_image = cv.filter2D(new_image,-1,kernel)
    
    else:
        new_image = image

    # Change image depth.
    if depth <= 0:
        new_image *= 0
    elif depth <= 8:
        new_image = np.floor(new_image/(2**(8-depth))) * 255/(2**depth)
    

    return new_image.astype(np.uint8)
                            

img = cv.imread('images/im1.jpg', 1)

new_img = im_chscaledepth(img, 1, 2)

cv.imshow('img', img)
cv.imshow('new_img', new_img)

cv.waitKey(0)